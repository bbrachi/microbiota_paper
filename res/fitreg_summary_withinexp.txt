year	location	N acc	r^2	selected linear terms	selected quandratic terms
2012	SU	196	0.262981892194251	+
B53
(***)/+
F8
(***)	+F8_squared(*)
2013	SU	198	0.102408522662022	+
F8
(***)	
2012	SR	177	0.0363363045696381	+
B13
(**)	+B13_squared(ns)
2013	SR	197	0.199652595061445	+
B25
(ns)/+
B26
(*)/-
B41
(*)/+
F160
(ns)/-
F5
(***)/+
F60
(*)/+
F8
(**)	-B41_squared(.)/+F8_squared(*)
2013	NA	197	0.0289737484922892	+
B26
(.)	+F150_squared(*)
2012	NM	160	0.0127250955771051		+B28_squared(.)
2013	NM	198	0.0576423117393241	-
F60
(***)/-
F69
(*)/+
F8
(*)	
