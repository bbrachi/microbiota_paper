source activate qiime2-2019.1

mkdir ./swarm_cseq
mkdir ./repseq

##get rep seqs from SWARM
cp ~/data/sweden/microbiota/swarm/16S/swarm_out/representative_seq_sub.fasta ./repseq/16S_rep_seq_sub.fasta
cp ~/data/sweden/microbiota/swarm/ITS/swarm_out/representative_seq_sub.fasta ./repseq/ITS_rep_seq_sub.fasta


####################################################
###############      ITS     #######################
####################################################

repseqITS="./repseq/ITS_rep_seq_sub.fasta"

taxUNITE="/home/benjamin/data/sweden/microbiota/tax/databases/UNITE/sh_taxonomy_qiime_ver8_dynamic_02.02.2019.txt"
seqUNITE="/home/benjamin/data/sweden/microbiota/tax/databases/UNITE/sh_refs_qiime_ver8_dynamic_02.02.2019.fasta"

##convert the lower case caracters to upper case in UNITE dev fasta file

##tr 'agct' 'AGCT' < $seqUNITE > $seqUNITE

qiime tools import \
  --type 'FeatureData[Sequence]' \
  --input-path $seqUNITE \
  --output-path ./qiime_files/seq_UNITE_99.qza

qiime tools import \
  --type 'FeatureData[Taxonomy]' \
  --input-format HeaderlessTSVTaxonomyFormat \
  --input-path $taxUNITE \
  --output-path ./qiime_files/tax_UNITE_99.qza


# qiime feature-classifier extract-reads \
#   --i-sequences 85_otus.qza \
#   --p-f-primer GTGCCAGCMGCCGCGGTAA \
#   --p-r-primer GGACTACHVGGGTWTCTAAT \
#   --p-trunc-len 120 \
#   --p-min-length 100 \
#   --p-max-length 400 \
#   --o-reads ref-seqs.qza

##create classifier (quite slow)
qiime feature-classifier fit-classifier-naive-bayes \
  --i-reference-reads ./qiime_files/seq_UNITE_99.qza \
  --i-reference-taxonomy ./qiime_files/tax_UNITE_99.qza \
  --o-classifier ./qiime_files/UNITE_classifier.qza

##test the classifier

head -n 300 $seqUNITE > UNITE_test_set.fasta
qiime tools import \
  --type 'FeatureData[Sequence]' \
  --input-path UNITE_test_set.fasta \
  --output-path ./qiime_files/testset_UNITE_99.qza


qiime feature-classifier classify-sklearn \
  --i-classifier ./qiime_files/UNITE_classifier.qza \
  --i-reads ./qiime_files/testset_UNITE_99.qza \
  --o-classification ./qiime_files/test_UNITE_classifier_taxonomy.qza

qiime tools export \
  --input-path ./qiime_files/test_UNITE_classifier_taxonomy.qza \
  --output-path test_UNITE_classifier_taxonomy


mkdir res
head -n 1000 $repseqITS | tr "atgc" "ATGC" > ITS_test_set.fasta
qiime tools import \
  --type 'FeatureData[Sequence]' \
  --input-path ITS_test_set.fasta \
  --output-path ./qiime_files/ITS_testset.qza

qiime feature-classifier classify-sklearn \
  --i-classifier ./qiime_files/UNITE_classifier.qza \
  --i-reads ./qiime_files/ITS_testset.qza \
  --o-classification ./qiime_files/ITS_testset_taxonomy.qza

qiime tools export \
  --input-path ./qiime_files/ITS_testset_taxonomy.qza \
  --output-path ./res/ITS_testset_taxonomy.txt


##do the whole set

mkdir res
cat $repseqITS | tr "atgc" "ATGC" > ITS_repseq.fasta
qiime tools import \
  --type 'FeatureData[Sequence]' \
  --input-path ITS_repseq.fasta \
  --output-path ./qiime_files/ITS_repseq.qza

qiime feature-classifier classify-sklearn \
  --i-classifier ./qiime_files/UNITE_classifier.qza \
  --i-reads ./qiime_files/ITS_repseq.qza \
  --o-classification ./qiime_files/ITS_repseq_taxonomy.qza

qiime tools export \
  --input-path ./qiime_files/ITS_repseq_taxonomy.qza \
  --output-path ./res/ITS_repseq_taxonomy

####################################################
###############      16S     #######################
####################################################

tax="/home/benjamin/data/sweden/microbiota/tax/databases/SILVA/SILVA_132_QIIME_release/taxonomy/16S_only/97/consensus_taxonomy_all_levels.txt"
seq="/home/benjamin/data/sweden/microbiota/tax/databases/SILVA/SILVA_132_QIIME_release/rep_set/rep_set_16S_only/97/silva_132_97_16S.fna"

##convert the lower case caracters to upper case in UNITE dev fasta file
##tr 'agct' 'AGCT' < $seqUNITE > $seqUNITE

qiime tools import \
  --type 'FeatureData[Sequence]' \
  --input-path $seq \
  --output-path ./qiime_files/seq_SILVA97.qza

qiime tools import \
  --type 'FeatureData[Taxonomy]' \
  --input-format HeaderlessTSVTaxonomyFormat \
  --input-path $tax \
  --output-path ./qiime_files/tax_SILVA97.qza


qiime feature-classifier extract-reads \
  --i-sequences ./qiime_files/seq_SILVA97.qza \
  --p-f-primer AACMGGATTAGATACCCKG \
  --p-r-primer ACGTCATCCCCACCTTCC \
  --p-trunc-len 251 \
  --p-min-length 100 \
  --p-max-length 400 \
  --o-reads ./qiime_files/truncated_seq_SILVA97.qza

##create classifier (quite slow)
qiime feature-classifier fit-classifier-naive-bayes \
  --i-reference-reads ./qiime_files/truncated_seq_SILVA97.qza \
  --i-reference-taxonomy ./qiime_files/tax_SILVA97.qza \
  --o-classifier ./qiime_files/SILVA_classifier97.qza

mkdir temp

##test the classifier
##export the trimed seq, subset and re-import
qiime tools export \
  --input-path ./qiime_files/truncated_seq_SILVA97.qza \
  --output-path ./temp/truncated_seq_SILVA97

head -n 300 ./temp/truncated_seq_SILVA97/dna-sequences.fasta > ./temp/SILVA_test_set.fasta
qiime tools import \
  --type 'FeatureData[Sequence]' \
  --input-path ./temp/SILVA_test_set.fasta \
  --output-path ./qiime_files/SILVA_test_set.qza

qiime feature-classifier classify-sklearn \
  --i-classifier ./qiime_files/SILVA_classifier97.qza \
  --i-reads ./qiime_files/SILVA_test_set.qza \
  --o-classification ./qiime_files/test_SILVA_classifier_taxonomy.qza

qiime tools export \
  --input-path ./qiime_files/test_SILVA_classifier_taxonomy.qza \
  --output-path test_SILVA_classifier_taxonomy

mkdir res
repseq16S="./repseq/16S_rep_seq_sub.fasta"
head -n 500 $repseq16S | tr "atgc" "ATGC" > 16S_testset.fasta

qiime tools import \
  --type 'FeatureData[Sequence]' \
  --input-path 16S_testset.fasta \
  --output-path ./qiime_files/16S_testset.qza

qiime feature-classifier classify-sklearn \
  --i-classifier ./qiime_files/SILVA_classifier97.qza \
  --i-reads ./qiime_files/16S_testset.qza \
  --o-classification ./qiime_files/16S_testset_taxonomy_SILVA97.qza

qiime tools export \
  --input-path ./qiime_files/16S_testset_taxonomy_SILVA97.qza \
  --output-path ./res/16S_testset_taxonomy_SILVA97.txt

head ./res/16S_testset_taxonomy_SILVA97.txt

##do the whole set

mkdir res
cat $repseq16S | tr "atgc" "ATGC" > 16S_repseq.fasta
qiime tools import \
  --type 'FeatureData[Sequence]' \
  --input-path 16S_repseq.fasta \
  --output-path ./qiime_files/16S_repseq.qza

qiime feature-classifier classify-sklearn \
  --i-classifier ./qiime_files/SILVA_classifier97.qza \
  --i-reads ./qiime_files/16S_repseq.qza \
  --o-classification ./qiime_files/16S_repseq_taxonomy.qza

qiime tools export \
  --input-path ./qiime_files/16S_repseq_taxonomy.qza \
  --output-path ./res/16S_repseq_taxonomy

