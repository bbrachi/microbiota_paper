##
## GEMMA Version    = 0.97 (2018/01/03)
## GSL Version      = 2.3
## Eigen Version    = 3.3.2
## OpenBlas         = OpenBLAS 0.2.19  - NO_AFFINITY HASWELL
##   arch           = HASWELL
##   threads        = 28
##   parallel type  = threaded
##
## Command Line Input = gemma -gk 2 -bfile /group/bergelson-lab/project_sweden/sweden/genomes/002.Swedes220.SNPs/sweden_final -p /group/bergelson-lab/project_sweden/sweden/genomes/002.Swedes220.SNPs/fakephen.txt -o K_all_accessions 
##
## Date = Mon Jan 15 07:33:32 2018
##
## Summary Statistics:
## number of total individuals = 200
## number of analyzed individuals = 200
## number of covariates = 1
## number of phenotypes = 1
## number of total SNPs/var = 1035775
## number of analyzed SNPs/var = 1035775
##
## Computation Time:
## total computation time = 0.77 min 
## computation time break down: 
##      time on calculating relatedness matrix = 0.669 min 
##
