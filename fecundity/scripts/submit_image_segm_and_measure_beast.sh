#!/bin/bash

Folder=$1

FILES=$(ls $Folder/*.ppm)

mkdir  $Folder/submission_files
mkdir  $Folder/labels
mkdir  $Folder/res

printf "%b\n" "$FILES" >$Folder/submission_files/list_ppm.txt

N=$(ls $Folder/*.ppm | wc -l)

for (( i=1; i<=$N; i++ ))
do

echo "#!/bin/bash
#PBS -N image_processing_"$i"
#PBS -d "$Folder"/submission_files/
#PBS -j oe
#PBS -q short
#PBS -S /bin/bash" > $Folder/submission_files/submit_segmentation.sh

echo 'R --slave --vanilla --file=/home/bbrachi/sweden/photos_all/stems/2012/scripts/segment_images_and_measure_beast_06252013.R --args image='$i'>log_'$i'.txt'>> $Folder/submission_files/submit_segmentation.sh

qsub $Folder/submission_files/submit_segmentation.sh
sleep 5
done;




