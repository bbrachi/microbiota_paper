#!/bin/bash

Folder=$1

FILES=$(ls $Folder/*.NEF)

mkdir  $Folder/submission_files


printf "%b\n" "$FILES" >$Folder/submission_files/list_NEFs.txt

N=$(ls $Folder/*.ppm | wc -l)
j=1
for i in $FILES
do

echo "#!/bin/bash
#PBS -N image_conversion_"$j"
#PBS -d "$Folder"/submission_files/
#PBS -q short
#PBS -j oe
#PBS -S /bin/bash" > $Folder/submission_files/submit_conversion.sh

echo 'ufraw-batch --exposure 1.88 --contrast 1.5 --overwrite '$i''>> $Folder/submission_files/submit_conversion.sh
qsub $Folder/submission_files/submit_conversion.sh
((j++))
sleep 5
done;
