#!/bin/bash
##submission script to genotoul for permutation


cd ~/work/datenr
traits=$(cat traits.txt)

for trait in ${traits[@]}
do
echo "#!/bin/bash

#SBATCH -J "$trait"
#SBATCH -o output.out
#SBATCH -e error.out
#SBATCH -t 8:00:00
#SBATCH -c 40
#SBATCH --mem=40G
#SBATCH --mail-type=BEGIN,END,FAIL
#Purge any previous modules
module purge
#Load the application
module load system/singularity-3.6.4

singularity shell enrichment.sif
Rscript --vanilla enr_perm_cluster.R "$trait"
" > submit_enr_"$trait".sh

#sbatch submit_PAST_"$trait".sh

done
