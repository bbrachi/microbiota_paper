#!/bin/bash

echo $0\n$1\n$2\n$3\n$4\n$5\n$6\n$7\n

doR2=$1 
flowcell=$2
year=$3
gene=$4
wd=$5
start=$6
Nsamples=$7



#cd /groups/Bergelson/ben/sweden/miseq
#flowcell="AECNJ"
#year=2013
#gene="16S"
##wd=/group/bergelson-lab/bbrachi_data/sweden/microbiota/miseq/swarm/


module add fastx_toolkit/0.0.13
module add cutadapt/1.2.1
mkdir $wd
mkdir $wd/$gene
mkdir $wd/$gene/fastas
mkdir $wd/$gene/$flowcell
wd=$wd/$gene/$flowcell
cd $wd
mkdir $wd/fastq
mkdir $wd/fasta
src=/group/bergelson-lab/bbrachi_data/sweden/microbiota/miseq/
fastqsFolder=$src/runs/*-$flowcell/Data/Intensities/BaseCalls/
R1=($fastqsFolder/*-*-*R1_001.fastq.gz)
R2=($fastqsFolder/*-*-*R2_001.fastq.gz)

for r1 in ${R1[@]:$start:$Nsamples};do
    cp  $r1 $wd/fastq
    ##change the folder in the names of those files
    r1w=$wd/fastq/$(basename $r1)
    r1f=$(sed 's/.gz//g'<<< $r1w)
    o=$(sed 's/_R1_001.fastq//g'<<< $r1f)_$gene_$year
    gzip -fd $r1w
    if [ "$doR2" == "1" ]; then
	r2=$(sed 's/R1/R2/g'<<< $r1)
	cp  $r2 $wd/fastq
	r2w=$wd/fastq/$(basename $r2)
	r2f=$(sed 's/.gz//g'<<< $r2w)
	gzip -fd $r2w
	/home/bbrachi/bin/pear  -f $r1f -r $r2f -q 10 -o $o -j 4
	rm $r1f
	rm $r2f
    elif [ "$doR2" == "0" ]; then
	mv $r1f $o.assembled.fastq
    fi;
    F=(${r1f//\// })
    W=(${wd//\// })
    L1=${#F[@]}
    S=${F[$L1-1]}
    S2=(${S//_/ })
    S3=${S2[0]}
    sample=$(sed 's/-/-/g' <<< $S3)
    echo $sample
    if [[ "$gene" == 'ITS' ]]; then
    	##remove the forward primers and all preceeding bases
    	cutadapt -g CTTGGTCATTTAGAGGAAGTAA -m 100 -q 20 -o $o.assembled.fastq.temp  $o.assembled.fastq
    	mv $o.assembled.fastq.temp $o.assembled.fastq
    	##remove the reverse complement of the reverse primer and all
    	cutadapt -a GCATCGATGAAGAACGCAGC -m 100 -q 20 -o $o.assembled.fastq.temp $o.assembled.fastq
    	mv $o.assembled.fastq.temp $o.assembled.fastq
    fi
    if [[ "$gene" == '16S' ]]; then
    	##remove the forward primers and all preceeding bases
    	cat $o.assembled.fastq | cutadapt -g AACMGGATTAGATACCCKG - > $o.assembled.fastq.temp
    	mv $o.assembled.fastq.temp $o.assembled.fastq
    	##remove the reverse complement of the reverse primer and all
    	cat $o.assembled.fastq | cutadapt -a GGAAGGTGGGGATGACGT - > $o.assembled.fastq.temp
    	mv $o.assembled.fastq.temp $o.assembled.fastq;
    fi
    ##fastx_quality_stats -i $o.assembled.fastq -o $wd/logs/$sample.assembled_fastx.txt -Q33
    fastq_quality_filter -q 30 -p 90 -i $o.assembled.fastq -o $o.assembled.filt.fastq -Q33
    final=$(echo $o | sed -e 's/\/fastq/\/fasta/')
    fastq_to_fasta -i $o.assembled.filt.fastq -Q33 -o $final.fasta
    ##dereplicate the fasta file
    grep -v "^>" $final.fasta | \
	grep -v [^ACGTacgt] | sort -d | uniq -c | \
	while read abundance sequence ; do
	    hash=$(printf "${sequence}" | sha1sum)
	    hash=${hash:0:40}
	    printf ">%s_%d_%s\n" "${hash}" "${abundance}" "${sequence}"
	done | sort -t "_" -k2,2nr -k1.2,1d | \
        sed -e 's/\_/;\n/2' -e 's/\_/;size=/1'  > $final.dereplicated.fasta
    vsearch --uchime_denovo $final.dereplicated.fasta --chimeras $final.dereplicated.chimeras.fasta --nonchimeras $final.dereplicated.nochimeras.fasta --threads 4 --fasta_width 0
    ##remove none dereplicated files and fastq files
    rm $final.fasta
    ##now reformat for swarm.
    sed -e 's/;//g' -e 's/size=/\_/g' -i $final.dereplicated.nochimeras.fasta
    ##move to the analysis folder and rename
    final2=$(echo $final | sed 's/'$flowcell'\/fasta/fastas/g')
    mv $final.dereplicated.nochimeras.fasta $final2.dereplicated.fasta
done;


