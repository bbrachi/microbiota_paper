#!/usr/bin/env bash
pref=$1
out=$2

cat $pref.blocks | sed 's/\*//g' | awk 'BEGIN{OFS="\t"}{for(i=1; i <= NF; i++)print NR,$i}' | gzip -c > $out.blocks.df.gz
