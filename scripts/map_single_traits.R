##bbrachi 08/03/2020
##mapping single traits: accession effects on hubs
##and fecundity across experiments.

## ---- loadlibgwa

library(plyr)
library(readr)
source("./scripts/GWA_functions.R")

library(foreach)
library(doParallel)

set.seed(123)

Ncores=7 ## number of cores for the GWAs, 1 core is used per trait using foreach. 

## ---- end-of-loadlibgwa

## ---- prepmap

##prep the traits

fit=readRDS("./res/fecundity_blups_acrossexp.rds")
hubs=readRDS("./res/blups_hh_allexp_rand_int.rds")

colnames(hubs)[1]="id"


dat=na.omit(merge(fit, hubs, by="id"))
dat[,c(-1)]=scale(dat[,c(-1)]) ##scaled 

phen=dat
row.names(phen)=paste(phen[,1])
phen=phen[,-1]

##add a missing acc
pref<-"./GWA/snps/sweden_200.bimbam"

##make a phenotype file
acclist<-scan(paste(pref, ".acclist", sep=""), sep=",")


##make a table of blups in the same order as the genotype
##add rows for accessions with no phenotypes
a=row.names(phen)
ma=acclist[acclist%in%a==F]
add=as.data.frame(matrix(NA, ncol=ncol(phen), nrow=length(ma)))
row.names(add)=paste(ma)
colnames(add)=colnames(phen)
phen=rbind(phen, add)
phen<-phen[paste(acclist),]
phenfile<-paste(pref, "_blups_fec_hh.phen.txt", sep="")
write.table(phen, phenfile, sep="\t", row.names=F, col.names=F)

## ---- end-of-prepmap

## ---- singlemap
system("mkdir ./GWA/output_gemma_loco/")
system(paste("gemma-wrapper --loco --json --cache-dir ./GWA/.gemma-cache -- -gk -g ", pref, ".geno.gz -a ", pref, ".map -p ", phenfile, " > ./GWA/output_gemma_loco/K.json", sep=""))


##run on Ncores. 
njobs=ncol(phen)
cl<-makeCluster(Ncores)
registerDoParallel(cl)
foreach(j=1:njobs)%dopar%{
    pheno<- colnames(phen)[j]
    x=paste("gemma-wrapper --loco --json --cache-dir ./GWA/.gemma-cache --input ./GWA/output_gemma_loco/K.json -- -lmm 4 -g ", pref, ".geno.gz -a ", pref, ".map -p ", phenfile, " -n ", j , " > ./GWA/output_gemma_loco/out_loco_", pheno,sep="")
    system(x)
}

stopCluster(cl)



## ---- end-of-singlemap


## ---- makemanhattan

##make manhattan plots

ph2=data.frame(phen=colnames(phen), ph2=NA)
gwsignif=list()

for(n in 1:ncol(phen)){
    pheno=paste(colnames(phen)[n], sep="")
    x=unlist(strsplit(scan(paste("./GWA/output_gemma_loco/out_loco_", pheno,sep=""), what="character")[1], ","))[7]
    f=substring(gsub("]]", "", x), 2, (nchar(x)-3))
    f2=gsub("assoc", "log", f)
    ph2$ph2[n]=ph2gemma(f2)
    p=read.delim(f, header=T, sep="\t")
    ##make a manhattan plot
    colnames(p)[3]="pos"
    colnames(p)=gsub("p_lrt", "pval", colnames(p))
    p$score=-log10(p$pval)
    p=p[p$af>=0.1,]
    p$fdr_pval=p.adjust(p$pval, "fdr")
    gwsignif[[pheno]]=p[p$fdr_pval<=0.05,]
    jpeg(paste("./GWA/manhattan/lmm_gwa_blups_", pheno, ".jpeg", sep=""),res=600, unit="in", width=8, height=4, pointsize=12)
    manhattan(p)
    dev.off()
    if(n==1){
        beta=p[, c("rs","beta")]; colnames(beta)=c("rs", pheno)
        se=p[, c("rs", "se")]; colnames(se)=c("rs", pheno)
    }else{
        b=p[, c("rs","beta")];colnames(b)=c("rs", pheno)
        beta=merge(b, beta, by="rs", all=T)
        s=p[, c("rs","se")]; colnames(s)=c("rs", pheno)
        se=merge(s, se, by="rs", all=T)
    }
}


saveRDS(ph2, paste("./res/ph2_gemma_hh_fec.rds", sep=""))
saveRDS(gwsignif, paste("./res/gw_signif_snps.rds", sep=""))
colnames(beta)=c("rs", colnames(phen))
colnames(se)=c("rs", colnames(phen))
row.names(beta)=beta$rs
row.names(se)=se$rs
beta2=beta[, -1]
se2=se[,-1]
saveRDS(beta2, "./GWA/beta_hh_fec.rds")
saveRDS(se2, "./GWA/se_hh_fec.rds")

gwsignif=readRDS(paste("./res/gw_signif_snps.rds", sep=""))

##make a barplot for ph2 with CI

K=matrix(scan("./GWA/K_200/K_all_accessions.cXX.txt", sep="\t"), ncol=200)
fam=read.table("./GWA/snps/sweden_200.fam")
colnames(fam)[1]="id"
colnames(K)=fam$id
row.names(K)=fam$id
##compute eigen values and save them
ED=eigen(K, symmetric=T)
E=ED$values
V=ED$vectors
cov=data.frame(matrix(c(rep(1, nrow(fam)), rep(0, nrow(fam))), ncol=2, nrow=nrow(fam)))
write.table(data.frame(E),"./GWA/fiesta/eigenvalues_K.txt", col.names=F, row.names=F, quote=F)
write.table(data.frame(V),"./GWA/fiesta/eigenvectors_K.txt", col.names=F, row.names=F, quote=F)
write.table(cov,"./GWA/fiesta/covariates_albi.txt", col.names=F, row.names=F, quote=F)
ph2=readRDS(paste("./res/ph2_gemma_hh_fec.rds", sep=""))
write.table(data.frame(ph2[,2]), "./GWA/fiesta/ph2_hh_allexp_for_albi_v2.txt", col.names=F, row.names=F, quote=F)

system(paste("python3 /usr/local/lib/python3.5/dist-packages/albi_lib/fiesta.py --kinship_eigenvalues ./GWA/fiesta/eigenvalues_K.txt -f ./GWA/fiesta/ph2_hh_allexp_for_albi_v2.txt -o ./GWA/fiesta/ph2_hh_allexp_CI_albi_v2.txt -n 1000 -c 0.95"))
##run command line outside of R.
ph2albi=read.table("./GWA/fiesta/ph2_hh_allexp_CI_albi_v2.txt", sep="\t", h=T)
ph2=cbind(ph2, ph2albi[,2:3])
ph2=ph2[order(ph2$ph2, decreasing=T),]

pdf("./figures/ph2_v2.pdf", paper="special", height=3, width=4, pointsize=8)
par(mar=c(5, 4, 1, 1))
b=barplot(ph2$ph2,names.arg=ph2$phen, col="Dodgerblue", las=2, ylim=c(0, 1))
segments(b, ph2[,3], b, ph2[,4])
dev.off()

## ---- end-of-makemanhattan

