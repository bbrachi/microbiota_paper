#!/bin/bash
wd="/home/bbrachi/work/booth2"


rm $wd/logs/*

for gene in "ITS" "16S"
do
    for e in "ull" "rat" "ram" "ada"
    do
	for y in 2012 2013
	do
	    for slice in $(seq 1 40)
	    do
		echo "#!/bin/bash

#SBATCH -o "$wd"/logs/booth2_"$e"_"$y"_"$gene".log
#SBATCH	-e "$wd"/logs/booth2_"$e"_"$y"_"$gene".err
#SBATCH --mem=8G
#SBATCH -c 8
#SBATCH -t 00:10:00
#SBATCH -J booth2_"$e"_"$y"_"$gene"

module load compiler/gcc-7.2.0
module load system/R-3.6.2_gcc-7.2.0

Rscript --vanilla "$wd"/scripts/boot_rep_batch.R "$gene" "$e" "$y" "$slice" > "$wd"/logs/R_output_boot_rep_batch_"$gene"_"$e"_"$y".txt" > $wd"/submit_h2boot.sh"
	    sbatch $wd"/submit_h2boot.sh"
	    rm $wd"/submit_h2boot.sh"
	    sleep 0.2s
	    done
	done
    done
done

