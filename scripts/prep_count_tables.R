##script to prep the count tables

library(readr)

filters=data.frame(matrix(ncol=4, nrow=6))
colnames(filters)=c("gene", "filter", "samples","OTUs")
fill=1

for(gene in c("ITS", "16S")){
    print(paste("processing countable for ", gene))

    ##read-in the data
    s=read_delim(paste("./data/SAMPLE-MAPPING-all.txt", sep=""), col_names=T, delim="\t")

    ##make a year column from the sample name.
    l=nchar(paste(s$samples))
    s$year=substring(paste(s$samples), l-3, l)

    ##add columns
    s[, c("plate", "row", "col")]=t(apply(s, 1, function(x){y=unlist(strsplit(x[1], "-"))[1:3]; return(y)}))
    s[, c("year", "plate", "col")]=apply(s[, c("year", "plate", "col")], 2, as.numeric)

    ##flip the coordinates of certain plates that look like they were flipped
    ##This modifications were determined by looking at the PcoA plots for each plates.
    L=list(c(2012,33), c(2012, 6), c(2012, 14))
    for(l in L){
        sub=s[s$plate==l[2] & s$year==l[1],]
        sub$flipped=paste(sub$plate,sub$row, 13-sub$col,sub$year,sep="-")
        sub$new_acc=sub$acc[match(sub$flipped, sub$samples)]
        sub$new_exp=sub$exp[match(sub$flipped, sub$samples)]
        sub$new_block=sub$block[match(sub$flipped, sub$samples)]
        s$acc[s$plate==l[2] & s$year==l[1]]=sub$new_acc
        s$exp[s$plate==l[2] & s$year==l[1]]=sub$new_exp
        s$block[s$plate==l[2] & s$year==l[1]]=sub$new_block
    }

    ##read a count table. The commented part is really slow. Reading the rdata file is slightly faster

    ##f=paste("./swarm/",gene, "/OTU_table.csv", sep="")
    ##ct=read_csv(f, col_names=T) ##quite slow
    ##saveRDS(ct, paste("./data/OTU_table_", gene, ".rdata", sep=""))

    ct=as.data.frame(readRDS(paste("./data/OTU_table_", gene, ".rdata", sep="")))

    ##fill the table with initial counts:
    filters[fill, "gene"]=gene
    filters[fill, "filter"]="initial"
    filters[fill, "samples"]=ncol(ct)
    filters[fill, "OTUs"]=nrow(ct)
    fill=fill+1

    ##order the samples as in the mapping file

    samples=colnames(ct)[-1]
    OTUs=ct[,1]
    ct=ct[,-1]

    samples=gsub("_S[0-9]+_L001_", "_", samples)
    samples=gsub("_", "-", samples)
    colnames(ct)=samples

    row.names(ct)=OTUs
    s=droplevels(s[match(paste(samples), paste(s$samples)),])

    ##remove the empty wells from ct, make a count table of empty wells and save it
    empties_ct=ct[, (s$exp=="empty" & is.na(s$exp)==F)]
    empties_s=droplevels(s[(s$exp=="empty" & is.na(s$exp)==F),])
    saveRDS(empties_s, paste("./data/empties_s_", gene, ".rds", sep=""))
    saveRDS(empties_ct, paste("./data/empties_ct_", gene, ".rds", sep=""))
    rm(empties_s)
    rm(empties_ct)

    ##remove empties from ct and s
    s=droplevels(s[s$exp!="empty",])
    s=na.omit(s)
    ##remove lines of s that are not in ct
    ct=ct[, colnames(ct)%in%s$samples]
    ct=ct[,match(paste(s$samples), colnames(ct))]

     ##fill the table with initial counts:
    filters[fill, "gene"]=gene
    filters[fill, "filter"]="empties"
    filters[fill, "samples"]=ncol(ct)
    filters[fill, "OTUs"]=nrow(ct)
    fill=fill+1

    ##match s again
    s=s[match(colnames(ct), s$samples),]

    ##read the list of accessions id to convert the 1-200 accession number to ids
    acclist=read.table("./data/acc_list.txt", h=T, se="\t")
    ##add ids to rs
    s$acc=gsub("acc_", "", s$acc)
    s$id= acclist$lines[match(s$acc, acclist$tubes)]
    s$region= acclist$region[match(s$acc, acclist$tubes)]
    s=as.data.frame(s)

    ##remove accessions with no ids or from Scotland (sampling errors)
    ct=ct[,s$id!="." & s$region%in%c("N Sweden", "S Sweden", "C Sweden")]
    s=droplevels(s[s$id!="." & s$region%in%c("N Sweden", "S Sweden", "C Sweden"),])

    ##fill the summary table:
    filters[fill, "gene"]=gene
    filters[fill, "filter"]="non_swedes"
    filters[fill, "samples"]=ncol(ct)
    filters[fill, "OTUs"]=nrow(ct)
    fill=fill+1

    ##match s again
    s=s[match(colnames(ct), s$samples),]

    ##remove poorly sequenced samples (less than 1000)

    col.sum=colSums(ct)
    print(paste("There are", length(col.sum[col.sum<=1000]), "samples with less than 1000 reads. Those samples are removed"))
    ct=ct[,col.sum>=1000]


    ##fill the summary table:
    filters[fill, "gene"]=gene
    filters[fill, "filter"]="(count/sample)>1000"
    filters[fill, "samples"]=ncol(ct)
    filters[fill, "OTUs"]=nrow(ct)
    fill=fill+1

    ##match s again
    s=s[match(colnames(ct), s$samples),]

    ##prefilter to 50 counts per OTU
    r=rowSums(ct)
    filter=r>50
    ct=ct[filter, ]

    ##fill the summary table:
    filters[fill, "gene"]=gene
    filters[fill, "filter"]="(count/OTU)>50"
    filters[fill, "samples"]=ncol(ct)
    filters[fill, "OTUs"]=nrow(ct)
    fill=fill+1

    ##here I save ct temporarely to free RAM
    save(ct, file=paste("./count_table_temp_", gene, ".rdata", sep=""))

    ##remove rare OTUs: those with less than 10 sequences in 5 samples
    temp=as.matrix(ct)
    rm(ct) ##make space

    temp[temp<10]=0
    temp[temp>=10]=1
    freq=rowSums(temp)
    rm(temp)

    ##Here I reload the temporally saved ct
    load(file=paste("./count_table_temp_", gene, ".rdata", sep=""))
    ##And here I remove the rare OTUs from ct
    ct=ct[freq>=5,]

    ##fill the summary table:
    filters[fill, "gene"]=gene
    filters[fill, "filter"]="(10seqin5samples/OTU)"
    filters[fill, "samples"]=ncol(ct)
    filters[fill, "OTUs"]=nrow(ct)
    fill=fill+1

    ##for the 16S data remove the OTUs that were assigned to mitochondria

    if(gene=="16S"){
        tax=readRDS(file=paste("./data/taxonomy_otus_", gene, "_v2.rds", sep=""))
        r=paste(na.omit(tax$OTU[tax[,6]=="mitochondria"]))
        n=row.names(ct)
        n=gsub("OTU[0]*","B", n)
        ct=ct[n%in%r==F,]
        print(paste("There are ", length(n[n%in%r]), "OTUs assigned to mitochondria in the 16S count table. Those are removed."))
        rm(tax)
        rm(n)
        ##fill the summary table:
        filters[fill, "gene"]=gene
        filters[fill, "filter"]="Assigned to chloroplast"
        filters[fill, "samples"]=ncol(ct)
        filters[fill, "OTUs"]=nrow(ct)
        fill=fill+1
    }


    ##remove poorly sequenced samples (less than 1000) again because by removing rare OTUs we could have made a few samples below 1000
    col.sum=colSums(ct)
    ct=ct[,col.sum>=1000]

    ##fill the summary table:
    filters[fill, "gene"]=gene
    filters[fill, "filter"]="(count/sample)>1000, again"
    filters[fill, "samples"]=ncol(ct)
    filters[fill, "OTUs"]=nrow(ct)
    fill=fill+1

    ##match s again
    s=droplevels(s[match(colnames(ct), s$samples),])

    print(paste("The final number of samples in the", gene, "count table is", ncol(ct)))
    print(paste("The final number of OTUs in the", gene, "count table is", nrow(ct)))

    ##make ct a simple data.frame and transpose
    ct=as.data.frame(t(ct))

    ##make format row.names into F1 and B1 instead of OTU00001
    if(gene=="16S"){colnames(ct)=gsub("OTU[0]*","B", colnames(ct))
    }else{colnames(ct)=gsub("OTU[0]*","F", colnames(ct))}

    ##save
    saveRDS(ct, file=paste("./data/ct_no_outliers_", gene, ".rds", sep=""))
    saveRDS(s, file=paste("./data/s_no_outliers_", gene, ".rds", sep=""))
    kable(table(s$exp, s$year), caption=paste("Number of samples per experiment for ", gene))
}


##save the table giving the filtering details
write.table(filters, paste("./res/filtering_counts.txt",sep=""),col.names=T, row.names=F, quote=F, sep="\t")
kable(filters, caption=paste("Details of the filtering steps"))



## genes=c("ITS","16S")
## for(gene in genes){
##     ct=readRDS(file=paste("./data/ct_no_outliers_", gene, ".rds", sep=""))
##     r=rowSums(ct)
##     print(paste(gene, median(r)))
## }


