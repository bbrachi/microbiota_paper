##bbrachi 17/04
##I want to make a little sh script using plink to compute LD blocks, Identify tag SNPs, and use that to get candidate genes.

outpre=$1
bedpre=$2
maxkb=$3
Ncores=$(cat /proc/cpuinfo | grep processor | wc -l)
# echo $Ncores
let Ucores=$Ncores-2
# echo $Ucores
plink --bfile $bedpre --chr 1-5 --blocks "no-pheno-req" -blocks-max-kb $maxkb --blocks-strong-lowci 0.7005 --blocks-strong-highci 0.9800005 --threads $Ucores --out $outpre
##--no-pheno --nonfounders --allow-no-sex
##notes about --blocks-strong-lowci 0.7005  and --block-strong-highci 0.980005, these have the default values, which center the LD at about 0.8, which is fine. the addition of 0.00...005 is circumvent a little problem in plink which doesn't include the outer limits of CIs. see plink website.


