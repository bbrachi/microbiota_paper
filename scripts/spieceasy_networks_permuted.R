#/usr/bin/R

##In this script the goal is to assess the robustness of the relationship between heritability and distance to hubs.
##Specifically we want to keep genotype effects intact, but remove microbe/microbes interactions
##The idea is to permute the dataset within host genotypes before computing networks.
##This should change the topology of the network. in particular it should remove all microbe-microbe interactions.
#and the correlation between h2 and distance to hubs should disappear, of at least not decrease in a linear fashion anymore.

## ---- permutednetworks


library(SpiecEasi)
library(igraph)
library(plyr)
set.seed(123)
exps=c("ull", "rat", "ram", "ada")
years=c(2012,2013)
genes=c("16S", "ITS")


change_exp_names=function(x){
    exps1=c("ull", "rat", "ram", "ada")
    exps2=c("SU", "SR", "NM", "NA")##c("S1", "S2", "N1", "N2")
    for(i in 1:4){
        x=gsub(exps1[i], exps2[i], x)
    }
    return(x)
}

setwd("~/data/sweden/microbiotav2")
s1=readRDS(file=paste("./data/s_no_outliers_16S.rds", sep=""))
s2=readRDS(file=paste("./data/s_no_outliers_ITS.rds", sep=""))
s1$plate=paste("B", s1$plate, sep="")
s2$plate=paste("F", s2$plate, sep="")
s=unique(rbind(s1, s2))
s=s[s$samples%in%s1$samples & s$samples%in%s2$samples,]
ct1=readRDS(file=paste("./data/ct_no_outliers_16S.rds", sep="", collapse="_"))
ct2=readRDS(file=paste("./data/ct_no_outliers_ITS.rds", sep="", collapse="_"))
#ct=cbind(ct1[paste(s$samples),],ct2[paste(s$samples),])
##only keep samples present in both data sets
ct1=ct1[paste(s$samples),]
ct2=ct2[paste(s$samples),]

##subset by experiment and compute new networks with permuted data.

for(e in exps){
    for(y in years){
        subct1=ct1[s$exp==e & s$year==y,]
        subct2=ct2[s$exp==e & s$year==y,]
        tot=sum(subct1)
        rs=colSums(subct1)
        subct1=subct1[, rs>=(0.0001*tot)]
        tot=sum(subct2)
        rs=colSums(subct2)
        subct2=subct2[, rs>=(0.0001*tot)]
        ##concatenate the columns of the fungal and bacterial count tables
        subct=as.matrix(cbind(subct1, subct2))
        subs=droplevels(s[s$exp==e & s$year==y,])
        ##do the permutations for all columns, within
        X=data.frame(acc=subs$acc, subct)
        R=ddply(X, .variable="acc", function(x){apply(x[,-1], 2, sample, replace=F)})
        subctr=as.matrix(R[,-1])
        print(paste(paste(genes, collapse="_"), e, y,paste(dim(subct), collapse=" ")))
        se.mb <- spiec.easi(subctr, method='mb', lambda.min.ratio=1e-2, nlambda=50)
        saveRDS(se.mb, paste("./res/spieceasy_model_", paste(genes, collapse="_"),"_", e, "_", y,"_v2_permuted.rds", sep=""))
    }
}

## ---- end-of-permutednetworks

## ---- permutednetworksstats

##compute stats for the networks
count=1
for(e in exps){
    for(y in years){
      subct1=ct1[s$exp==e & s$year==y,]
      subct2=ct2[s$exp==e & s$year==y,]
      tot=sum(subct1)
      rs=colSums(subct1)
      subct1=subct1[, rs>=(0.0001*tot)]
      tot=sum(subct2)
      rs=colSums(subct2)
      subct2=subct2[, rs>=(0.0001*tot)]
      ##concatenate the columns of the fungal and bacterial count tables
      subct=as.matrix(cbind(subct1, subct2))
      #tot=sum(subct)
      #rs=colSums(subct)
      #subct=subct[, rs>=(0.0001*tot)]
      #subs=droplevels(s[s$exp==e & s$year==y,])
      otus=colnames(subct)
      se.mb=readRDS(paste("./res/spieceasy_model_", paste(genes, collapse="_"),"_", e, "_", y,"_v2_permuted.rds", sep=""))
      ig.mb <- graph.adjacency(se.mb$refit$stars, mode='undirected')
      vertex_attr(ig.mb)$name=otus#colnames(se.mb$data)
      g=igraph::simplify(ig.mb)
      saveRDS(g, paste("./res/graph_", e, "_", y, "_permuted.rds", sep=""))
      ##otus=vertex_attr(g)$name
      deg=degree(g)
      btw=betweenness(g, directed=F)
      mb=cluster_edge_betweenness(g)
      if(count==1){
        stats=data.frame(exp=e, year=y, otus=otus, deg=deg, btw=btw, mb=mb$membership)
      }else{
        stats=rbind(stats,data.frame(exp=e, year=y, otus=otus, deg=deg, btw=btw, mb=mb$membership))}
        count=count+1
      }
    }
saveRDS(stats, "./res/stats_spieceasy_networks_v2_permuted.rds")

## ---- end-of-permutednetworksstats

## ---- netcomp

###Combine the new stats with the old ones to represent the change in network configuration.
stats=readRDS("./res/stats_spieceasy_networks_v2.rds")
statsp=readRDS("./res/stats_spieceasy_networks_v2_permuted.rds")

colnames(statsp)[4:6]=paste(colnames(stats)[4:6], "p", sep="")

Nstats=merge(stats, statsp, by=c("exp", "year", "otus"))
Nstats$exp=change_exp_names(Nstats$exp)
##show that networks are different/similar and that hubs are the same/diff

library(ggplot2)
library("ggpubr")
theme_set(
  theme_bw() +
    theme(legend.position = "top")
  )

# p=ggplot(Nstats, aes(x=btw, y=btwp, color=exp))+geom_point(size=2, shape=16)+facet_grid(rows = vars(exp), cols = vars(year))+labs(x = "Betweeness No Permutation")+labs(y = "Betweeness after permutation within genotypes")
# p

# p2=ggplot(Nstats, aes(x=deg, y=degp, color=exp))+geom_point(size=2, shape=16)+facet_grid(rows = vars(exp), cols = vars(year))+labs(x = "Betweeness No Permutation")+labs(y = "Betweeness after permutation within genotypes")
# p2

##define hubs in both cases (permuted and not permuted)
thh=0.95##quantile for betweenness and deg
hubs=list()
hubsp=list()
exps=c("SU", "SR", "NA", "NM")

pdf(paste("./figures/hubs_ITS_16S_v2_with_permutations.pdf", sep=""), height=8, width=5, paper="special", pointsize=8)
par(mfrow=c(4, 2), mar=c(3, 3.5, 1, 1), mgp=c(2, 0.8, 0))
for(e in exps){
    for(y in years){
        sub=Nstats[Nstats$exp==e & Nstats$year==y,]
        qbtw=quantile(sub$btw, thh)
        qdeg=quantile(sub$deg, thh)
        qbtwp=quantile(sub$btwp, thh)
        qdegp=quantile(sub$degp, thh)
        col=c("firebrick2", "gold","darkblue",  "dodgerblue")[match(e,c("SU", "SR", "NA", "NM"))]
        colt=apply(sapply(col, col2rgb)/255, 2, function(x)(rgb(x[1], x[2], x[3], alpha=0.35)))
        cols=rep(colt, nrow(sub))
        cols[sub$btw>=qbtw & sub$deg>=qdeg]=col
        #cols[outlier.scores>=qo]=col
        #cols[is.na(outlier.scores)]="red"
        cexs=rep(1, nrow(sub))
        cexs[cols==col]=3
        ##identify hubs in both normal and permuted data
        cols[(sub$btw>=qbtw & sub$deg>=qdeg) & (sub$btwp>=qbtwp & sub$degp>=qdegp)]="grey60"
        plot(sub$btw, sub$deg, xlab="betweenness", ylab="degree", pch=16, col=cols, cex=cexs, cex.lab=1.5, cex.axis=1.5)
        #points(sub$btwp, sub$degp, pch=17, col=cols, cex=cexs)
        legend("bottomright",paste(e, y), cex=2, bty="n")
        text(x=sub$btw[sub$btw>=qbtw & sub$deg>=qdeg], y=sub$deg[sub$btw>=qbtw & sub$deg>=qdeg], labels=paste(sub$otus[cols==col]), cex=0.6, col="white")
        ##fill the list of hubs
        hubs[[paste("ITS_16S", e, y, sep="_")]]=sub$otus[sub$btw>=qbtw & sub$deg>=qdeg]
        hubsp[[paste("ITS_16S", e, y, sep="_")]]=sub$otus[sub$btwp>=qbtwp & sub$degp>=qdegp]

      }
}

dev.off()


## ---- end-of-netcomp

