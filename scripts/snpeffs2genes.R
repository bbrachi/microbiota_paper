
## ---- snp2genes

##27042020
##bbrachi: transfering SNP effects to annotated genes within LD blocks
#libraries
library(tidyr)
library(readr)
library(stringr)
library(plyr)
library(dplyr)
library(GenomicRanges)
##set upt ddply to run on multicores
library(doMC)
doMC::registerDoMC(cores=5)

bf="/home/benjamin/data/sweden/microbiotav2/"
##hard coded path to files
bedpref="/home/benjamin/data/sweden/microbiotav2/GWA/snps/sweden_200"
gff_file="/home/benjamin/data/sweden/microbiotav2/data/TAIR10_GFF3_genes.gff"
pathwayfile="/home/benjamin/data/sweden/microbiotav2/data/aracyc_pathways.20180702"
##
mbeta=readRDS("./GWA/beta_hh_fec.rds")
mse=readRDS("./GWA/se_hh_fec.rds")

##saveRDS(se, "./GWA/mashr/se_hh_fec.rds")
##read mashr effects
#m.1by1=readRDS("./GWA/mashr/mashr_1by1_hh_fec_v4.rds")
## pbeta=as.data.frame(m.1by1$result$PosteriorMean)
## pse=as.data.frame(m.1by1$result$PosteriorSD)
## traits=colnames(pbeta)

###functions

readGWA=function(GWAout, blocks, bf="/home/benjamin/data/sweden/microbiotav2/"){
  gwafile=scan(GWAout, what="character")%>%
  strsplit("," ) %>% .[[1]] %>%
  unlist(.) %>% .[7] %>%
  str_replace("]]", '') %>%
  str_replace('[.]/', bf) %>%
  str_sub(.,2) %>% str_sub(., star=1, end=-2)
  gwa=read_delim(gwafile, delim="\t", col_names=T, col_types='ccddccddddddddd')
  ##now we need to add info about LD blocks
  gwa=merge(gwa, blocks, "rs")
  gwa=arrange(gwa, chr, ps)
  return(gwa)
}

collapseff=function(x){
  ##find tag snps per block
  L=nrow(x); we=which.max(abs(x$beta))[1];wa=which.min(x$p_wald)[1]; s=sign(x$beta);ss=sum(s==s[wa])
  ##in case two snps have the same largest effet exactly... take the first one.
  out= data.frame(chr=paste("Chr", x$chr[1], sep=""), start=min(x$ps), end=max(x$ps), x[we,c("rs", "ps", "af", "beta", "se", "p_wald")], nsnps=nrow(x))
  if((L==1) | (sum(s)==L) | (sum(s)==(-1*L)) | (ss>=(L/2))){ ##one snps in the block of all snps the same sign keep the snp with the largest effect
    return(out)}else{if(L>1 & ss<(L/2)){out$beta=(-1)*out$beta; return(out)}}}


collapseff2=function(x){#This on function only keeps the highest absolute effect.
      ##find tag snps per block
      L=nrow(x); we=which.max(abs(x$beta))[1];wa=which.min(x$p_wald)[1]; s=sign(x$beta);ss=sum(s==s[wa])
      ##in case two snps have the same largest effet exactly... take the first one.
      out= data.frame(chr=paste("Chr", x$chr[1], sep=""), start=min(x$ps), end=max(x$ps), x[we,c("rs", "ps", "af", "beta", "se", "p_wald")], nsnps=nrow(x))
      out$beta=abs(out$beta); return(out)}

lookForOverlap=function(genes, ggwa){
  x=findOverlaps(genes, ggwa)
  x=as.data.frame(x)
  a=as.data.frame(genes, stringsAsFactors=F)
  b=as.data.frame(ggwa, , stringsAsFactors=F)
  a=a[x[,1],c("gene")]
  b=b[x[,2],]
  res=cbind(gene=a, b)
  return(res)
}



##reading the files needed for all traits a gwa file for example (will be an input for the function)
##LD blocks
blocks=read_delim(paste(bedpref, ".blocks.df.gz", sep=""), delim="\t", col_names=F)
colnames(blocks)=c("block", "rs")
##the candidate genes.
full_gff <- as.data.frame(rtracklayer::readGFF(gff_file,
       filter = list(type = "gene"))) %>% select(.data$seqid,
       .data$start, .data$end, .data$Name)
genes=with(full_gff, GRanges(seqid, IRanges(start=start, end=end), gene=Name))

##analysis for each trait
traits=colnames(mbeta)
write.table(traits, "./data/traits.txt", col.names=F, row.names=F, quote=F, sep="\t")

                                        #pbeta$rs=row.names(mbeta)

for(trait in traits){
##setting up variables for dev
##trait="B38"
    GWAout=paste("/home/benjamin/data/sweden/microbiotav2/GWA/output_gemma_loco/out_loco_", trait, sep="")
##read GWA, add block info
    gwa=readGWA(GWAout, blocks)
    df1=data.frame(rs=row.names(mbeta), beta=mbeta[,trait], se=mse[,trait])
    gwa2=merge(gwa[, colnames(gwa)%in%c("beta","se")==F] , df1, by="rs", all.y=F, all.x=F, sort=F)
    ##now for each block, get the largest absolute effect and it's range in chr, and pos.
    beff=ddply(gwa2, "block", collapseff2, .parallel=T) ##collapseff2 only keeps the absolute effect values.
##make into a genomic rage object
    ggwa=with(beff, GRanges(chr, IRanges(start=start, end=end), block=block,rs=rs, ps=ps, af=af, beta=beta, pv=p_wald, nsnps=nsnps))
##find out which block overlaps with which genes
  res=lookForOverlap(genes, ggwa)
##collapse to a table similar to the imput of PAST...
  final=ddply(res, "gene", function(x){ w=which.max(abs(x$beta));
      return(data.frame(chromosome=x$seqnames[w], position=x$ps[w], marker=x$rs[w], effect=x$beta[w], p.value=x$pv[w], linked_snp_count=sum(x$nsnps)))}, .parallel=T)
  final=arrange(final, chromosome, position)
##save the final table.
  saveRDS(final, paste("./res/genes_for_enrichement_", trait, "_abseff.rds", sep=""))
}

## ---- end-of-snp2genes


##In the next section I try to remove genes within the same blocks that have the same pathway annotations

## ---- snp2genes2

                                        #pbeta$rs=row.names(mbeta)


pathwayfile="/home/benjamin/data/sweden/microbiotav2/data/aracyc_pathways.20180702"
pw <- read.table(pathwayfile, sep = "\t", header = TRUE, quote = "")
spw=unique(pw[,c("Gene.id", "Pathway.id")])


for(trait in traits){
##setting up variables for dev
##trait="B38"
    GWAout=paste("/home/benjamin/data/sweden/microbiotav2/GWA/output_gemma_loco/out_loco_", trait, sep="")
    ##read GWA, add block info
    gwa=readGWA(GWAout, blocks)
    #df1=data.frame(rs=row.names(mbeta), beta=mbeta[,trait], se=mse[,trait])
    #gwa2=merge(gwa[, colnames(gwa)%in%c("beta","se")==F] , df1, by="rs", all.y=F, all.x=F, sort=F)
    #rm("gwa", "df1") 
    ##now for each block, get the largest absolute effect and it's range in chr, and pos.
    beff=ddply(gwa, "block", collapseff2, .parallel=T) ##collapseff2 only keeps the absolute effect values.
    ##make into a genomic rage object
    ggwa=with(beff, GRanges(chr, IRanges(start=start, end=end), block=block,rs=rs, ps=ps, af=af, beta=beta, pv=p_wald, nsnps=nsnps))
    ##find out which block overlaps with which genes
    res=lookForOverlap(genes, ggwa)
    rm(ggwa, beff, gwa, gwa2, df1)
    
    ##thin genes too keep one per pathway annotation per block
    res2=droplevels(merge(res, spw , by.x="gene", by.y="Gene.id",all.x=T))
    thinned=ddply(res2[1:1000,], c("block", "Pathway.id"), function(x){return(x[1,])})
       
    
    ##collapse to a table similar to the imput of PAST...
    final=ddply(res, "gene", function(x){ w=which.max(abs(x$beta));
        return(data.frame(chromosome=x$seqnames[w], position=x$ps[w], block=x$block[w],marker=x$rs[w], effect=x$beta[w], p.value=x$pv[w], linked_snp_count=sum(x$nsnps)))}, .parallel=T)
    final2=droplevels(merge(final, spw , by.x="gene", by.y="Gene.id",all.x=T))

    library(doMC)
    doMC::registerDoMC(cores=5)

    final3=ddply(final2, c("block", "Pathway.id"), function(x){x[1,]}, .parallel=T)
    

        final=arrange(final, chromosome, position)
##save the final table.
  saveRDS(final, paste("./res/genes_for_enrichement_", trait, "_abseff_thinned.rds", sep=""))
}




## ---- end-of-snp2genes2


