##load the count tables

## ---- herit0

library(vegan)
library(MASS)
library(mixOmics)
library(plyr)
source("./scripts/heritability_functions.R")

##set up loops
genes=c("ITS", "16S")
exps=c("ull", "rat", "ada", "ram")
years=c(2012, 2013)

##load the taxonomy
tb=readRDS("./data/taxonomy_otus_16S_v2.rds")
tf=readRDS("./data/taxonomy_otus_ITS_v2.rds")
tax=rbind(tb, tf)


## ---- end-of-herit0

## ---- herit1
##compute the heritabilities of individual OTUs

Notus=data.frame()
count=1
uotus=c()
for(gene in genes){
    ct=readRDS(file=paste("./data/ct_no_outliers_", gene, ".rds", sep=""))
    s=readRDS(file=paste("./data/s_no_outliers_", gene, ".rds", sep=""))
    s$year=as.factor(s$year)
    s$plate=as.factor(s$plate)
    for(e in exps){
        for(y in years){
            subct=ct[s$exp==e & s$year==y,]
            subs=droplevels(s[s$exp==e & s$year==y,])
            tot=sum(subct)
            rs=colSums(subct)
            subct=as.matrix(subct[, rs>=(0.0001*tot)])
            uotus=c(uotus, colnames(subct))
            Notus[count,"exp"]=e
            Notus[count,"year"]=y
            Notus[count,"gene"]=gene
            Notus[count,"Notus"]=ncol(subct)
            Notus[count,"Nsamples"]=nrow(subct)
            r=apply(subct, 1, rank, ties="average")
            #Notus[count, "meanRank"]=t(apply(r, 2, mean))
            lsubct=logratio.transfo(subct+1, logratio="CLR", offset=0)
            h2=apply(lsubct,2, H2nokin, id=subs$id)
            saveRDS(h2, paste("./res/h2_", gene, "_", e, "_", y,".rds", sep=""))
            count=count+1
        }
    }
}
saveRDS(Notus, "./res/Notus_Nsamples.rds")

## ---- end-of-herit1

#####ADDITION FOR REV4, July 2021
###HOW MANY reads total before filter and how many is 0.01%

Notus=data.frame()
count=1
uotus=c()
for(gene in genes){
    ct=readRDS(file=paste("./data/ct_no_outliers_", gene, ".rds", sep=""))
    s=readRDS(file=paste("./data/s_no_outliers_", gene, ".rds", sep=""))
    s$year=as.factor(s$year)
    s$plate=as.factor(s$plate)
    for(e in exps){
        for(y in years){
            subct=ct[s$exp==e & s$year==y,]
            subs=droplevels(s[s$exp==e & s$year==y,])
            tot=sum(subct)
            rs=colSums(subct)
            subct=as.matrix(subct[, rs>=(0.0001*tot)])
            uotus=c(uotus, colnames(subct))
            Notus[count,"exp"]=e
            Notus[count,"year"]=y
            Notus[count,"gene"]=gene
            Notus[count,"Notus"]=ncol(subct)
            Notus[count,"Nsamples"]=nrow(subct)
            Notus[count, "reads"]=tot
            Notus[count, "filter"]=0.0001*tot
            count=count+1
        }
    }
}

#saveRDS(Notus, "./res/Notus_Nsamples.rds")


## ---- datasize1
set.seed(123)

Notus=readRDS("./res/Notus_Nsamples.rds")
kable(Notus, caption="Number of OTUs and number of samples for each location/year combination")

## ---- end-of-datasize1

## ---- datasize2
#print(paste("Nsamples on average=", mean(Notus$Nsamples), " / from= ", min(Notus$Nsamples), "/ to= ", max(Notus$Nsamples), sep=""))
kable(ddply(Notus, "gene", function(x){c(Av_Notus=mean(x$Notus), from=min(x$Notus), to=max(x$Notus))}), caption="Number of OTUs and samples in each experiment used on which heritability analysis were performed.")
## ---- end-of-datasize2

## ---- distherit

prop_herit=c()
pdf("./figures/otu_heritability_distribution.pdf", paper="special", height=6, width=4, pointsize=8)
## m=c(1,1,2,3,4,4,5,6,rep(1, 4), rep(4, 4))
## mall=rep(m, 4)
## add=sort(rep(seq(0, (6*3), 6), 16))
## mat=matrix(mall+add, ncol=8, byrow=T)
## layout(mat, width=c(1, 1, 3, 2, 1, 1, 3, 2), heights=rep(c(3, 1), 4))
m=c(1,1,2,3,3,4,rep(1, 3), rep(3, 3))
mall=rep(m, 4)
add=sort(rep(seq(0, (4*3), 4), 12))
mat=matrix(mall+add, ncol=6, byrow=T)
layout(mat, width=c(1, 1, 2, 1, 1, 2), heights=rep(c(3, 1), 4))
#layout.show(16)
count=1
for(e in exps){
    e2=c("SU", "SR", "NM", "NA")[match(e,c("ull", "rat", "ram", "ada"))]
    for(y in years){
        par(mar=c(3, 3, 3, 1), mgp=c(1.5, 0.6, 0))
        h2_1=readRDS(paste("./res/h2_ITS_", e, "_", y,".rds", sep=""))
        h2_2=readRDS(paste("./res/h2_16S_", e, "_", y,".rds", sep=""))
        h2boot_1=readRDS(paste("./res/h2boot_ITS_", e, "_", y,".rds", sep=""))
        h2boot_2=readRDS(paste("./res/h2boot_16S_", e, "_", y,".rds", sep=""))
        ##reconcile OTUs that are in one but not in the other
        h2_1=h2_1[names(h2_1)%in%colnames(h2boot_1)]
        h2boot_1=h2boot_1[, colnames(h2boot_1)%in%names(h2_1)]
        h2_2=h2_2[names(h2_2)%in%colnames(h2boot_2)]
        h2boot_2=h2boot_2[, colnames(h2boot_2)%in%names(h2_2)]
        h2boot_1=h2boot_1[, names(h2_1)]
        h2boot_2=h2boot_2[, names(h2_2)]
        h2=c(h2_1, h2_2)
        h2boot=cbind(h2boot_1, h2boot_2)
        x=data.frame(otus=names(h2),exp=e, year=y, h2=unlist(h2), medboot=unlist(apply(h2boot,2, median)), CIlow=unlist(apply(h2boot,2,  quantile, 0.025)), CIhigh=unlist(apply(h2boot,2, quantile, 0.975)))
        ##remove OTUs that have heritabilities outside of 95%confidence interval
        x=x[x$h2>=x$CIlow & x$h2<=x$CIhigh,]
        x$org=substring(x$otus, 1, 1)
        prop_herit[paste(e, y, sep="_")]=(100*(nrow(x[x$CIlow>0.001,])/nrow(x)))
        col=c("firebrick2", "gold","darkblue",  "dodgerblue")[match(e,c("ull", "rat", "ada", "ram"))]
        hist(x$h2, probability=T, xlim=c(0, 0.35), ylim=c(0, 70), main=paste(e2, y), breaks=seq(0, 0.35, by=0.01), xlab="heritability", ylab="frequency (bins of 1%)", col=col)
        text(0.22, 2, paste("% of OTUs with low CI\nabove 0.1%: ", round(prop_herit[paste(e, y, sep="_")], 2), "%", sep=""), adj=c(0, 0), cex=0.8, bty="o", col="firebrick3")
        box()
        par(mar=c(3,3, 3.5, 1), mgp=c(1.8, 0.6, 0))
        ## ed=ecdf(x$h2)
        ## plot(1-ed(x$h2), x$h2, main="", cex=0.5, pch=16, col="firebrick", cex.axis=0.8, las=2, xlab="quantiles", ylab="heritability", cex.lab=0.8)
        ## par(mar=c(3, 1.5, 3.5, 1.5), mgp=c(1.5, 0.6, 0))
        ## ##add example on ULL 2012.
        ## if(e=="ull" & y==2012){
        ##     hex=0.05
        ##     z=1-ed(hex) ##function of heritability
        ##     segments(z, -0.01, z, hex, lwd=0.8, lty=1, col="grey60")
        ##     segments(-0.01, hex,z,hex, lwd=0.8, lty=1, col="grey60")
        ## }
        #par(mar=c(3, 3, 3, 1), mgp=c(1.5, 0.6, 0))
        boxplot(h2~org, data=x, cex=0.5, pch="", lwd=0.5, cex.axis=0.8, las=2, xlab="")
        stripchart(h2~org, data=x, jitter=0.2, method="jitter", vertical=T, add=T, pch=16, col="Dodgerblue", cex=0.3)
        if(count==1){X=x}else{X=rbind(X, x)}
        count=count+1
    }
}
dev.off()
saveRDS(X, "./res/h2_ind_otus.rds")
saveRDS(prop_herit, "./res/prop_herit.rds")

## ---- end-of-distherit






