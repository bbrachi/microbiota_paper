#!/bin/bash

##bbrachi
##just a little script to submit PAST analysis on a cluster (cbib)

cd ~/work/running_PAST2
traits=$(cat traits.txt)

for trait in ${traits[@]}
do
echo "#!/bin/bash

#SBATCH --job-name=enrich_"$trait"
#SBATCH --time=5:00:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=10
#SBATCH --mem-per-cpu=10000MB

## Useful information to print
echo '########################################'
echo 'Date:' $(date --iso-8601=seconds)
echo 'User:' $USER
echo 'Host:' $HOSTNAME
echo 'Job Name:' $SLURM_JOB_NAME
echo 'Job Id:' $SLURM_JOB_ID
echo 'Directory:' $(pwd)
# Detail Information:
###scontrol show job $SLURM_JOB_ID
echo '########################################'
# modules loading
##module add R/3.6.1

module load compiler/gcc-7.2.0
module load system/R-3.6.2_gcc-7.2.0

Rscript --vanilla running_PAST_abseff.R $trait
echo '########################################'
echo 'Job finished' $(date --iso-8601=seconds)
" > submit_PAST_"$trait".sh
sbatch submit_PAST_"$trait".sh
done
