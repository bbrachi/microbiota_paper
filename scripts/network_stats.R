


## ---- netstats
library(igraph)
library(wordcloud)
library(readODS)

years=c(2012, 2013)
exps=c("ull", "rat", "ram", "ada")

stats=data.frame()
count=1
s1=readRDS(file=paste("./data/s_no_outliers_16S.rds", sep=""))
ct1=readRDS(file=paste("./data/ct_no_outliers_16S.rds", sep="", collapse="_"))
s2=readRDS(file=paste("./data/s_no_outliers_ITS.rds", sep=""))
ct2=readRDS(file=paste("./data/ct_no_outliers_ITS.rds", sep="", collapse="_"))
s=unique(rbind(s1, s2))
s=s[s$samples%in%s1$samples & s$samples%in%s2$samples,]
## ct=cbind(ct1[paste(s$samples), ], ct2[paste(s$samples),])
ct1=ct1[paste(s$samples),]
ct2=ct2[paste(s$samples),]


for(e in exps){
    for(y in years){
        subct1=ct1[s$exp==e & s$year==y,]
        subct2=ct2[s$exp==e & s$year==y,]
        tot=sum(subct1)
        rs=colSums(subct1)
        subct1=subct1[, rs>=(0.0001*tot)]
        tot=sum(subct2)
        rs=colSums(subct2)
        subct2=subct2[, rs>=(0.0001*tot)]
        ##concatenate the columns of the fungal and bacterial count tables
        subct=as.matrix(cbind(subct1, subct2))
        #tot=sum(subct)
        #rs=colSums(subct)
        #subct=subct[, rs>=(0.0001*tot)]
        #subs=droplevels(s[s$exp==e & s$year==y,])
        otus=colnames(subct)
        se.mb=readRDS(paste("./res/spieceasy_model_16S_ITS_", e, "_", y,"_v2.rds", sep=""))
        ig.mb <- graph.adjacency(se.mb$refit$stars, mode='undirected')
        vertex_attr(ig.mb)$name=otus#colnames(se.mb$data)
        g=igraph::simplify(ig.mb)
        saveRDS(g, paste("./res/graph_", e, "_", y, ".rds", sep=""))
                                        #otus=vertex_attr(g)$name
        deg=degree(g)
        btw=betweenness(g, directed=F)
        mb=cluster_edge_betweenness(g)
        if(count==1){
            stats=data.frame(exp=e, year=y, otus=otus, deg=deg, btw=btw, mb=mb$membership)
        }else{
            stats=rbind(stats,data.frame(exp=e, year=y, otus=otus, deg=deg, btw=btw, mb=mb$membership))}
        count=count+1
    }
}
saveRDS(stats, "./res/stats_spieceasy_networks_v2.rds")

##identify hubs
stats=readRDS("./res/stats_spieceasy_networks_v2.rds")
hubs=list()

thh=0.95

pdf(paste("./figures/hubs_ITS_16S_v2.pdf", sep=""), height=8, width=5, paper="special", pointsize=8)
par(mfrow=c(4, 2), mar=c(3, 3.5, 1, 1), mgp=c(2, 0.8, 0))
for(e in exps){
    for(y in years){
        sub=stats[stats$exp==e & stats$year==y,]
        qbtw=quantile(sub$btw, thh)
        qdeg=quantile(sub$deg, thh)
        col=c("firebrick2", "gold","darkblue",  "dodgerblue")[match(e,c("ull", "rat", "ada", "ram"))]
        colt=apply(sapply(col, col2rgb)/255, 2, function(x)(rgb(x[1], x[2], x[3], alpha=0.35)))
        cols=rep(colt, nrow(sub))
        cols[sub$btw>=qbtw & sub$deg>=qdeg]=col
        #cols[outlier.scores>=qo]=col
        #cols[is.na(outlier.scores)]="red"
        cexs=rep(1, nrow(sub))
        cexs[cols==col]=3
        e2=c("SU", "SR", "NM", "NA")[match(e,c("ull", "rat", "ram", "ada"))]
        plot(sub$btw, sub$deg, xlab="betweenness", ylab="degree", pch=16, col=cols, cex=cexs, cex.lab=1.5, cex.axis=1.5)
        legend("bottomright",paste(e2, y), cex=2, bty="n")
        text(x=sub$btw[cols==col], y=sub$deg[cols==col], labels=paste(sub$otus[cols==col]), cex=0.6, col="white")
        ##fill the list of hubs
        hubs[[paste("ITS_16S", e, y, sep="_")]]=sub$otus[cols==col]
    }
}
saveRDS(hubs, "./res/hubs_list_v2.rds")
dev.off()

## ---- end-of-netstats


## ---- hubs

##read the taxonomy assignations
tb=readRDS("./data/taxonomy_otus_16S_v2.rds")
tf=readRDS("./data/taxonomy_otus_ITS_v2.rds")
tax=rbind(tb, tf)

hubs=readRDS("./res/hubs_list_v2.rds")

x=table(droplevels(unlist(hubs)))
NF8=as.numeric(x["F8"])

subhubs=hubs[names(hubs)[grep("ITS_16S", names(hubs))]]
r=sort(table(paste(unlist(subhubs))))
r=r[r>0]
##print(paste(gene, collapse="_"))
tx=tax[match( names(r), tax$OTU),]
x2=data.frame(tx[, c(1, 5:7)], count=as.numeric(r)[r>0])
kable(x2, caption=paste("OTUs which are hubs in at least 2 experiments. Hubs are defined by belonging to the 95% tail of both the betweenness centrality and the degree."))

## ---- end-of-hubs


## ---- interkingdom

library(igraph)
res=data.frame(matrix(ncol=4, nrow=8))
colnames(res)=c("exp_year", "B_B", "B_F", "F_F")
count=0
for(e in exps){
    for(y in years){
        count=count+1
        g=readRDS(paste("./res/graph_", e, "_", y, ".rds", sep=""))
        l=get.edgelist(g)
        v=table(apply(l, 1, function(x){paste(substring(x, 1, 1), collapse="_")}))
        res[count, "edges"]="all"
        res[count,"exp_year"]=paste(e, y, sep="_")
        res[count, c("B_B", "B_F", "F_F")]= as.numeric(v[c("B_B", "B_F", "F_F")])
        count=count+1
        subhubs=paste(unlist(hubs[paste("ITS_16S_", e, "_", y, sep="")]))
        l2=l[apply(l, 1, function(x, h){any(x%in%h)==T}, h=subhubs),]
        v2=table(apply(l2, 1, function(x){paste(substring(x, 1, 1), collapse="_")}))
        res[count, "edges"]="withhubs"
        res[count,"exp_year"]=paste(e, y, sep="_")
        res[count, c("B_B", "B_F", "F_F")]= as.numeric(v2[c("B_B", "B_F", "F_F")])
        C=chisq.test(t(res[(count-1):count,2:4]), simulate.p.value=T, B=50000)
        res[(count-1),"chisq"]=C$statistic
        res[(count-1),"pval"]=C$p.value
        res[(count-1),"adjpval"]=p.adjust(C$p.value, method="BH", n=8)
        res[(count),"chisq"]=""
        res[(count),"pval"]=""
        res[(count),"adjpval"]=""
    }
}

res$exp_year=change_exp_names(res$exp_year)

write.table(res, "./res/chisq_interkingdom_v2.txt", col.names=T, row.names=F, quote=F)

write_ods(res, "./res/chisq_interkingdom_v2.ods")


prop_hubconn=100*apply(res[res$edges!="all", 2:4], 1, sum)/apply(res[res$edges=="all", 2:4], 1, sum)
prop_interk=100*apply(res[res$edges=="all", 2:4], 1, function(x){x[2]/sum(x)})

## print("Mean and range of proportions of edges between bacteria and fungus:")
## round(mean(prop_interk), 2)
## round(range(prop_interk), 2)


## print("Mean and range of proportions of edges involving at least one hub:")
## round(mean(prop_hubconn), 2)
## round(range(prop_hubconn), 2)

res[, 2:4]=t(apply(res[, 2:4], 1, function(x){x/sum(x)}))

##make a quick figure

x=res$exp_year

for(e in exps){
    x[grep(e, x)]=gsub(e, c("SU", "SR", "NM", "NA")[match(e,c("ull", "rat","ram", "ada"))], x[grep(e, x)])
}
res$exp_year=x



pdf("./figures/barplot_BB_FB_FF_edges_v2.pdf", paper="special", height=6, width=8)
par(mar=c(9, 3, 1, 1))
barplot(t(res[seq(1, nrow(res), by=1),2:4]), names.arg=paste(res$exp_year, res$edges, sep="_")[seq(1, nrow(res), by=1)],col=c("darkolivegreen", "Dodgerblue", "darkolivegreen3"), legend.text=c("bacterial", "interkingdom", "fungal"), bty="n", las=2, beside=F, ylim=c(0, 1.3), yaxt="n")
axis(2, at=seq(0, 1, by=0.2))
dev.off()

## ---- end-of-interkingdom




## ## ---- hubstats

## hubs=readRDS("./res/hubs_list_v2.rds")
## pan=readRDS("./res/pan_microbiota.rds")

## uh=unique(unlist(hubs))
## core=unique(pan[pan[,2]==8,1])
## uhc=uh%in%core

## hprev=pan[pan[,1]%in%uh,]
## pie(table(hprev[,2]))

## nhc=sum(uhc)


## x=table(droplevels(unlist(hubs)))


## hist(x)

## layout(matrix(1:4, ncol=2))
## hist(x)

## ## ---- end-of-hubstats
