#!/bin/sh

##bbrachi, 25/08/2020
##script to make README.Rmd in md doc with Rmarkdown

##deleted ods files from the res folder, the readODS library seems to have a problem overwriting files.  
#rm res/*.ods

#Rscript -e "rmarkdown::render('README.Rmd', output_format = 'md_document', output_file = 'README.md')"

#Rscript -e "rmarkdown::render('analysis.Rmd', output_file = 'analysis.html')"


##push to git

#mv README\[exported\].md README.md 
git add .gitignore
#git add analysis_files
git add .gitlab-ci.yml
git add analysis.html
git add README.md
git add figures
git add scripts
#git add submission
git add analysis.Rmd
git commit -m "updating readme and fixing pages"
git push



######################################################
#####################################################

##move tables and files to the submission folder

##script to move figures and supp figures/tables to the write folders with the write names in the submission folder.

##Main text figures
cp figures/cap2.jpg submission/figures/fig1.jpg
cp figures/otu_heritability_dist_perc_2013.pdf submission/figures/fig2.pdf
convert -quality 99 -density 300 submission/figures/fig2.pdf submission/figures/fig2.png
cp figures/fig3_hbs_fitreg2.pdf submission/figures/fig3.pdf

# ##Main Text Table(s)
##NONE

# ##Extended Data Figures

cp figures/taxdesc.pdf submission/extended_data/figures/Extended_Data_Fig1.pdf
cp figures/otu_heritability_dist_perc_2012.pdf submission/extended_data/figures/Extended_Data_Fig2.pdf
cp figures/meanRank_vs_h2.pdf submission/extended_data/figures/Extended_Data_Fig3.pdf
cp figures/hubs_ITS_16S_v2.pdf submission/extended_data/figures/Extended_Data_Fig4.pdf
cp figures/h2_vs_netstats.pdf submission/extended_data/figures/Extended_Data_Fig5.pdf
cp figures/Nexp_vs_h2.pdf submission/extended_data/figures/Extended_Data_Fig6.pdf
cp fecundity/figures/correlation_by_hand_by_surf.pdf submission/extended_data/figures/Extended_Data_Fig7.pdf
cp figures/fitness_pairwise_correlation_matrix.pdf submission/extended_data/figures/Extended_Data_Fig8.pdf
cp figures/specialized_metv4.pdf submission/extended_data/figures/Extended_Data_Fig9.pdf

# #Extended Data Tables

cp res/varexp_pcoa_within_v3.ods submission/extended_data/tables/Extended_Data_Table1.ods
cp res/heritable_hubs_v2.ods submission/extended_data/tables/Extended_Data_Table2.ods
cp res/chisq_interkingdom_v2.ods submission/extended_data/tables/Extended_Data_Table3.ods
cp res/fitreg_withinexp.ods submission/extended_data/tables/Extended_Data_Table4.ods
cp B38/data/field_collection_sites_B38.ods submission/extended_data/tables/Extended_Data_Table5.ods
##Extended data table 6 is build by hand. 

# # Supplementary Tables
# ##Supp Table 1 is hand made, the list of accessions
cp data/accessions.ods submission/supplementary_information/Supp_Table1.ods
cp res/taxonomy_all_OTUs.ods submission/supplementary_information/Supp_Table2.ods
cp res/qtls_local_score.ods  submission/supplementary_information/Supp_Table3.ods
cp res/go.ods submission/supplementary_information/Supp_Table4.ods
cp res/pathway.ods submission/supplementary_information/Supp_Table5.ods

