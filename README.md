This repository holds the analysis for 

# Plant genetic effects on microbial hubs impact fitness across field trials


Benjamin Brachi<sup>1,2</sup>, Daniele Filiault<sup>3\*</sup>, Hannah Whitehurst<sup>1\*</sup>, Paul Darme<sup>1</sup>, Pierre Le Gars<sup>1</sup>, Marine Le Mentec<sup>1</sup>, Timothy C. Morton<sup>1</sup>, Envel Kerdaffrec<sup>3</sup>, Fernando Rabanal<sup>3</sup>, Alison Anastasio<sup>1</sup>, Mathew S. Box<sup>4</sup>, Susan Duncan<sup>4</sup>, Feng Huang<sup>1,5</sup>, Riley Leff<sup>1</sup>, Polina Novikova<sup>3</sup>, Matthew Perisin<sup>1</sup>, Takashi Tsuchimatsu<sup>3</sup>, Roderick Woolley<sup>1</sup>, Caroline Dean<sup>4</sup>, Magnus Nordborg<sup>3</sup>, Svante Holm<sup>6</sup>, Joy Bergelson<sup>1</sup>

## Affiliations:

<sup>1</sup> Department of Ecology and Evolution, University of Chicago, Chicago, IL 60637

<sup>2</sup> Univ. Bordeaux, INRAE, BIOGECO, F-33610 Cestas, France

<sup>3</sup> Gregor Mendel Institute (GMI), Austrian Academy of Sciences, Vienna Biocenter (VBC), Dr. Bohr-Gasse 3, 1030 Vienna, Austria

<sup>4</sup> John Innes Center, Norwich, UK

<sup>5</sup> South China Botanical Garden, Chinese Academy of Sciences, Guangzhou, China

<sup>6</sup> Mid-Sweden University, Sundsvall, Sweden

<sup>*</sup>contributed equally to the work

## Corresponding author:

Joy Bergelson  jbergels@uchicago.edu

Regarding analysis: 

Benjamin Brachi benjamin.brachi@inrae.fr

## Abstract

Although complex interactions between hosts and microbial associates are increasingly well documented, we still know little about how and why hosts shape microbial communities in nature. In addition, host genetic effects on microbial communities vary widely depending on the environment, obscuring conclusions about which microbes are impacted and which plant functions are important. We characterized the leaf microbiota of 200 A. thaliana genotypes in eight field experiments and detected consistent host effects on specific, broadly distributed microbial OTU's. Host genetics disproportionately influenced hubs within the microbial communities, with their impact then percolating through the community, as evidenced by a decline in the heritability of particular OTUs with their distance to the nearest hub. By simultaneously measuring host performance, we found that host genetics associated with microbial hubs explained over 10% of the variation in lifetime seed production among host genotypes across sites and years. We successfully cultured one of these microbial hubs and demonstrated its growth-promoting effects on plants grown in sterile conditions. Finally, genome-wide association mapping identified many putatively causal genes with small effects on the relative abundance of microbial hubs across sites and years, and these genes were enriched for those involved in specialized metabolites synthesis, auxins and the immune system. Using untargeted metabolomics, we corroborate the consistent association of variation in specialized metabolites and microbial hubs across field sites. Together, our results reveal that host natural variation impacts the microbial communities in consistent ways across environments and that these effects contribute to fitness variation among host genotypes. 


## Reading and running analysis

All analysis are described on this page: https://bbrachi.pages.mia.inra.fr/microbiota_paper/analysis

Otherwise you can just clone the repository using 

```
git clone https://forgemia.inra.fr/bbrachi/microbiota_paper.git

```
