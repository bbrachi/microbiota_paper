---
title: "B38 Fitness Trials"
author: "Hannah Whitehurst"
date: "8/17/2020"
output:
  html_document:
    fig_caption: yes
    highlight: textmate
    theme: sandstone
    toc: yes
    toc_float: yes
    keep_md: true
  pdf_document:
    toc: yes
  word_document:
    toc: yes
---



## Overview

We isolated B38 from wild _A. thaliana_ leaves. We subsequently tested the growth effects of B38 inoculated on _A. thaliana_ (genotype 6136) plants compared to control plants (inoculation buffer only). 

The plants were individually photographed immediately before inoculation, then again at 7 and 14 days post-inoculation. The images were processed using cv2 in Python, which measured the plant material in each well. Final analysis reavealed that an substantial number of plants showed signs of water logged stress from the humidity in the plates and drip inoculation methods. 

Plants were scored in a manner by which the scorer was unaware of the treatment for each plant.

### Data description

We imaged each plant surface and measured surface areas. All measurements from the three trials are located in the B38_Fitness_Trials.csv file. Below is the description file data organization.

<table class="table" style="margin-left: auto; margin-right: auto;">
 <thead>
<tr><th style="border-bottom:hidden; padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="2"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Data file organization</div></th></tr>
  <tr>
   <th style="text-align:left;"> data column names </th>
   <th style="text-align:left;"> description </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> trial </td>
   <td style="text-align:left;"> trial containing sample (trial_a, trial_b, trial_c) </td>
  </tr>
  <tr>
   <td style="text-align:left;"> plate </td>
   <td style="text-align:left;"> sample's plate identity </td>
  </tr>
  <tr>
   <td style="text-align:left;"> position </td>
   <td style="text-align:left;"> categorical position of the sample well (middle, edge, corner) </td>
  </tr>
  <tr>
   <td style="text-align:left;"> row </td>
   <td style="text-align:left;"> row letter of the sample well (A:D) </td>
  </tr>
  <tr>
   <td style="text-align:left;"> column </td>
   <td style="text-align:left;"> column number of the sample well (1:6) </td>
  </tr>
  <tr>
   <td style="text-align:left;"> sample </td>
   <td style="text-align:left;"> unique sample ID </td>
  </tr>
  <tr>
   <td style="text-align:left;"> treatment </td>
   <td style="text-align:left;"> which treatment the sample received; C (control), I(infected), X(not included in experiment) </td>
  </tr>
  <tr>
   <td style="text-align:left;"> condition </td>
   <td style="text-align:left;"> scoring post experiment on the condition of the plant </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Time_0 </td>
   <td style="text-align:left;"> plant surface area immediately before treatment </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Time_1 </td>
   <td style="text-align:left;"> plant surface area ~7 days post inoculation (mm^2) </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Time_2 </td>
   <td style="text-align:left;"> plant surface area ~14 days post inoculation (mm^2) </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Time_0.1 </td>
   <td style="text-align:left;"> new plant surface area measured between time points 0 and 1 (mm^2) </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Time_1.2 </td>
   <td style="text-align:left;"> new plant surface area measured between time points 1 and 2 (mm^2) </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Time_0.2 </td>
   <td style="text-align:left;"> new plant surface area measured between time points 0 and 2 (mm^2) </td>
  </tr>
</tbody>
</table>




### Scoring description

The plants were scored for stunted growth and water-logged traits. The scores are provided in the data table in the "condition" column. Below is a description of each annotation used. Only the recovered plants were used for the subsequent analysis. 


<table class="table" style="margin-left: auto; margin-right: auto;">
 <thead>
<tr><th style="border-bottom:hidden; padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="2"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">Plant scoring: annotations and parameters</div></th></tr>
  <tr>
   <th style="text-align:left;"> annotation </th>
   <th style="text-align:left;"> parameters </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> o </td>
   <td style="text-align:left;"> no observation of waterlogged phenotypes or stunted growth (i.e. 'OK') </td>
  </tr>
  <tr>
   <td style="text-align:left;"> w </td>
   <td style="text-align:left;"> white </td>
  </tr>
  <tr>
   <td style="text-align:left;"> s </td>
   <td style="text-align:left;"> stunted </td>
  </tr>
  <tr>
   <td style="text-align:left;"> p </td>
   <td style="text-align:left;"> partial recovery: waterlogged phenotype, less than 50% of leaves translucent </td>
  </tr>
  <tr>
   <td style="text-align:left;"> m </td>
   <td style="text-align:left;"> mostly recovered : waterlogged phenotype, but less than 25% of leaves are translucent </td>
  </tr>
  <tr>
   <td style="text-align:left;"> l </td>
   <td style="text-align:left;"> waterlogged phenotype: curled leaves, bushy middle, 1-3 true leaves </td>
  </tr>
  <tr>
   <td style="text-align:left;"> u </td>
   <td style="text-align:left;"> ungerminated </td>
  </tr>
</tbody>
</table>




## Growth summary


![](B38_fitness_trials2019[exported]_files/figure-html/overview-1.png)<!-- -->![](B38_fitness_trials2019[exported]_files/figure-html/overview-2.png)<!-- -->

B38 infected plants were observed to have 10.5% more surface area than control plants. The average size of a control plant and B38 infected plant is approximately 54.35 and 60.74 mm^2, respectively. 



## LMER analysis

```
## Linear mixed model fit by REML ['lmerMod']
## Formula: Time_0.2 ~ treatment + (1 | trial/plate)
##    Data: healthy
## 
## REML criterion at convergence: 6228.7
## 
## Scaled residuals: 
##      Min       1Q   Median       3Q      Max 
## -2.87145 -0.64621  0.00916  0.66858  3.10953 
## 
## Random effects:
##  Groups      Name        Variance Std.Dev.
##  plate:trial (Intercept)  78.29    8.848  
##  trial       (Intercept) 416.57   20.410  
##  Residual                588.78   24.265  
## Number of obs: 670, groups:  plate:trial, 62; trial, 3
## 
## Fixed effects:
##             Estimate Std. Error t value
## (Intercept)   52.603     11.933   4.408
## treatmentI     5.375      1.973   2.725
## 
## Correlation of Fixed Effects:
##            (Intr)
## treatmentI -0.089
```

```
## Analysis of Deviance Table (Type II Wald F tests with Kenward-Roger df)
## 
## Response: Time_0.2
##                F Df Df.res   Pr(>F)   
## treatment 7.3981  1 652.33 0.006703 **
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
```

![](B38_fitness_trials2019[exported]_files/figure-html/unnamed-chunk-1-1.png)<!-- -->


